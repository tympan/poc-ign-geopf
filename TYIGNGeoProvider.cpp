#include "TYIGNGeoProvider.h"
#include "TYWebSocketTransport.h"
#include "TYGeographicData.h"

#include <QQmlComponent>
#include <QQuickItem>
#include <QQmlContext>
#include <QQuickWindow>

TYIGNGeoProvider* TYIGNGeoProvider::singleton_= nullptr;

TYIGNGeoProvider::TYIGNGeoProvider(/*QObject* parent*/)
{
    _engine = new QQmlApplicationEngine();
    _isInitialised = false;
}

TYIGNGeoProvider* TYIGNGeoProvider::getInstance(/*QObject* parent*/)
{
    if(singleton_==nullptr){
        singleton_ = new TYIGNGeoProvider();
    }
    return singleton_;
}

void TYIGNGeoProvider::initEngine()
{
    Q_INIT_RESOURCE(qml);
    // Register types to use them in QML
    qmlRegisterType<TYWebSocketTransport>("codetympan.org", 1, 0, "WebSocketTransport");
    qmlRegisterType<TYGeographicData>("codetympan.org", 1, 0, "GeographicData");
    _engine->load(QUrl(QStringLiteral("qrc:/TYZoneSelectPage.qml")));
    QList<QObject*> rootObjects = _engine->rootObjects();
    if (!rootObjects.isEmpty()) {
        QObject* mainComponent = rootObjects.at(0);
        QObject *geographicData = mainComponent->findChild<QObject*>("geographicData");
        if (geographicData)
        {
            QObject::connect(geographicData, SIGNAL(cancelRequested()), this, SLOT(handleCancelRequest()));
            QObject::connect(geographicData, SIGNAL(oKRequested()), this, SLOT(handleOKRequest()));
        }
    }

    _isInitialised = true;
}

void TYIGNGeoProvider::openModalWindow()
{
    // Get the root QML object
    QList<QObject*> rootObjects = _engine->rootObjects();
    if (!rootObjects.isEmpty()) {
        QObject* mainComponent = rootObjects.at(0);
        QQuickWindow *window = qobject_cast<QQuickWindow*>(mainComponent);
        window->show();

        qDebug() << "Window object name: " << window->objectName();
    }
}

void TYIGNGeoProvider::handleCancelRequest()
{
    // Handle the cancel request here
    // For example, emit a signal to indicate that the cancel action was triggered
    qDebug() << "Geo project creation cancelled";
    _engine->disconnect();
}

void TYIGNGeoProvider::handleOKRequest()
{
    qDebug() << "Geo project creation validated";
    // _engine->disconnect();
    emit geoProjectCreationRequested();
}
