// TYMainWindow.cpp

#include "TYMainWindow.h"
#include "TYIGNGeoProvider.h"
#include <QAction>
#include <QMenuBar>
#include <QQuickWindow>
#include <QQmlComponent>

TYMainWindow::TYMainWindow(QWidget *parent) : QMainWindow(parent) {
    // Set window properties
    setObjectName("Main Window");
    resize(1280, 1000);

    // Create the action for the menu
    bool ret = true;
    _pCreateNewAction = new QAction(tr("Create New Geo Project"), this);
    ret = ret && QObject::connect(_pCreateNewAction, &QAction::triggered, this, &TYMainWindow::showZoneSelectPage);

    // Add the action to the menu bar
    _pMenu = menuBar()->addMenu(tr("&File"));
    _pMenu->addAction(_pCreateNewAction);

    TYIGNGeoProvider* geoProvider = TYIGNGeoProvider::getInstance();
    QObject::connect(geoProvider, &TYIGNGeoProvider::geoProjectCreationRequested,
                     this, &TYMainWindow::createNewGeoProject);

    qDebug() << "Connect returns " << ret;
}

TYMainWindow::~TYMainWindow() {
    // Cleanup if necessary
}

void TYMainWindow::showZoneSelectPage() {
    if (!TYIGNGeoProvider::getInstance()->isInitialised())
    {
        TYIGNGeoProvider::getInstance()->initEngine();
    }
    TYIGNGeoProvider::getInstance()->openModalWindow();
}

void TYMainWindow::createNewGeoProject() {
    qDebug() << "Do create new geo project";
}
