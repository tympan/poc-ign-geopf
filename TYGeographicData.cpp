#include "TYGeographicData.h"

TYGeographicData::TYGeographicData(QObject *parent) : QObject(parent)
{
    connect(this, &TYGeographicData::backgroundImgChanged, this, &TYGeographicData::saveImageToFileSlot);
    connect(this, &TYGeographicData::levelCurvesChanged, this, &TYGeographicData::saveLevelCurvesToFileSlot);
}

void TYGeographicData::setLandtakeCoordinates(const QString &text)
{
    if (text != m_landtake_coordinates) {
        m_landtake_coordinates = text;
        emit landtakeCoordinatesChanged();
    }
}

QString TYGeographicData::crs() const
{
    return m_crs;
}

void TYGeographicData::setCrs(const QString &text)
{
    if (text != m_crs) {
        m_crs = text;
        emit crsChanged();
    }
}

QString TYGeographicData::landtakeCoordinates() const
{
    return m_landtake_coordinates;
}

void TYGeographicData::setBackgroundImg(const QString &base64Image)
{
    if (base64Image != m_base64Image) {
        m_base64Image = base64Image;
        m_image = QImage();
        emit backgroundImgChanged();
    }
}

QString TYGeographicData::backgroundImg() const
{
    return m_base64Image;
}

QString TYGeographicData::levelCurves() const
{
    return m_level_curves;
}

void TYGeographicData::setLevelCurves(const QString &text)
{
    if (text != m_level_curves) {
        m_level_curves = text;
        emit levelCurvesChanged();
    }
}

QImage TYGeographicData::getImage() const
{
    if (m_image.isNull() && !m_base64Image.isEmpty()) {
        m_image = buildImageFromBase64(m_base64Image);
    }
    return m_image;
}

bool TYGeographicData::saveImageToFile(const QString &filePath)
{
    QImage image = getImage();
    if (image.isNull()) {
        qWarning() << "Image is null, cannot save.";
        return false;
    }

    // Save the image to a PNG file
    return image.save(filePath, "PNG");
}

bool TYGeographicData::saveXmlFile(const QString &fileName)
{
    // Create a file object
    QFile file(fileName);

    // Open the file in write-only mode
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "Failed to open file for writing:" << file.errorString();
        return false;
    }

    // Create a QTextStream to write to the file
    QTextStream out(&file);

    // Write the XML string to the file
    out << m_level_curves;

    // Close the file
    file.close();

    qDebug() << "XML file saved successfully:" << fileName;
    return true;
}

void TYGeographicData::saveImageToFileSlot()
{
    QString filePath = "C:\\projects\\tympan\\issues\\512 - POC\\image.png"; // Choose a file path where the image will be saved
    if (!saveImageToFile(filePath)) {
        qWarning() << "Failed to save image to file:" << filePath;
    }
}

void TYGeographicData::saveLevelCurvesToFileSlot()
{
    QString filePath = "C:\\projects\\tympan\\issues\\512 - POC\\level_curves.xml"; // Choose a file path where the image will be saved
    if (!saveXmlFile(filePath)) {
        qWarning() << "Failed to save level curves to file:" << filePath;
    }
}

QImage TYGeographicData::buildImageFromBase64(const QString &base64Image) const
{
    // Decode base64 image string to byte array
    QByteArray imageData = QByteArray::fromBase64(base64Image.toUtf8());

    // Load byte array into a QImage
    QImage image;
    image.loadFromData(imageData);

    return image;
}
