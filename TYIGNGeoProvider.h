#ifndef TY_IGN_GEO_PROVIDER_H
#define TY_IGN_GEO_PROVIDER_H

#include <QObject>
#include <QQuickWindow>
#include <QQmlApplicationEngine>

class TYIGNGeoProvider : public QObject
{
    Q_OBJECT

private:
    TYIGNGeoProvider();

    static TYIGNGeoProvider* singleton_;

    QQmlApplicationEngine* _engine;

    bool _isInitialised;

public:

    TYIGNGeoProvider(TYIGNGeoProvider &other) = delete;
    void operator=(const TYIGNGeoProvider &) = delete;

    static TYIGNGeoProvider *getInstance();

    void initEngine();

    bool isInitialised()
    {
        return _isInitialised;
    }

    void openModalWindow();

signals:
    void geoProjectCreationRequested();

private slots:
    void handleCancelRequest();
    void handleOKRequest();
};

#endif // TY_IGN_GEO_PROVIDER_H
