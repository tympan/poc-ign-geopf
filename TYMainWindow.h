// TYMainWindow.h

#ifndef TYMAINWINDOW_H
#define TYMAINWINDOW_H

#include <QMainWindow>

class TYMainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit TYMainWindow(QWidget *parent = nullptr);
    ~TYMainWindow();

private slots:
    void showZoneSelectPage();
    void createNewGeoProject();

private:
    QAction* _pCreateNewAction;
    QMenu* _pMenu;
        // Add private members or methods here
};

#endif // TYMAINWINDOW_H
