#ifndef TYGEOGRAPHICDATA_H
#define TYGEOGRAPHICDATA_H

#include <QImage>
#include <QObject>
#include <qqml.h>
#include <QXmlStreamWriter>
#include <QFile>
#include <QTextStream>

class TYGeographicData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString landtake_coordinates READ landtakeCoordinates WRITE setLandtakeCoordinates NOTIFY landtakeCoordinatesChanged)
    Q_PROPERTY(QString background_img READ backgroundImg WRITE setBackgroundImg NOTIFY backgroundImgChanged)
    Q_PROPERTY(QString crs READ crs WRITE setCrs NOTIFY crsChanged)
    Q_PROPERTY(QString level_curves READ levelCurves WRITE setLevelCurves NOTIFY levelCurvesChanged)
    QML_ELEMENT
public:
    explicit TYGeographicData(QObject *parent = nullptr);

    void setLandtakeCoordinates(const QString &text);
    QString crs() const;
    void setCrs(const QString &text);
    QString landtakeCoordinates() const;
    void setBackgroundImg(const QString &base64Image);
    QString backgroundImg() const;
    QString levelCurves() const;
    void setLevelCurves(const QString &text);
    QImage getImage() const;
    Q_INVOKABLE bool saveImageToFile(const QString &filePath);
    Q_INVOKABLE bool saveXmlFile(const QString &fileName);

signals:
    void landtakeCoordinatesChanged();
    void crsChanged();
    void backgroundImgChanged();
    void levelCurvesChanged();

private slots:
    void saveImageToFileSlot();
    void saveLevelCurvesToFileSlot();

private:
    QString m_landtake_coordinates;
    QString m_crs;
    QString m_base64Image;
    QString m_level_curves;
    mutable QImage m_image;

    QImage buildImageFromBase64(const QString &base64Image) const;
};

#endif // TYGEOGRAPHICDATA_H
