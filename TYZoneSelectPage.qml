import QtQuick 2.15
import QtQuick.Window 2.15
import QtWebView 1.1
import QtWebSockets 1.1
import QtWebChannel 1.0
import QtQuick.Controls 2.15
import QtQuick.LocalStorage 2.0
import codetympan.org 1.0

Window {
    id: modalWindow
    width: 1600
    height: 950
    visible: true
    modality: Qt.ApplicationModal
    title: qsTr("POC Qt IGN-Geoplateforme")

    GeographicData {
        id: geographicData
        objectName: "geographicData"
        WebChannel.id: "core"
        function receiveCoordinates(text){
            console.log("receiveCoordinates: ", text)
            geographicData.landtake_coordinates = text
        }
        signal sendCoordinates(string text)
        signal cancelRequested()
        signal oKRequested()
        function receiveCrs(crs){
            console.log("receiveCrs: ", crs)
            geographicData.crs = crs
        }
        function receiveImg(img){
            console.log("receiveImg")
            geographicData.background_img = img
        }
        function receiveLevelCurves(level_curves){
            console.log("receiveLevelCurves")
            geographicData.level_curves = level_curves
        }
        function cancel() {
            geographicData.cancelRequested();
            modalWindow.close();
        }
        function view_send_ok() {
            geographicData.oKRequested();
            modalWindow.close();
        }

    }

    Text {
        id: landtakeInput
        width: parent.width - 20 // Adjust width to fit the window width
        text: geographicData.landtake_coordinates + " " + geographicData.crs
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10 // Add margin to the top and sides
    }

    WebView {
        id: webView
        url: "qrc:/TYZoneSelectPage.html"
        width: parent.width - 20 // Adjust width to fit the window width
        height: parent.height - landtakeInput.height - 40 // Adjust height to fit WebView below TextField
        anchors.top: landtakeInput.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10 // Add margin to the top and sides
    }

    WebSocketTransport{
        id: transport
    }

    WebSocketServer {
        listen: true
        port: 49327
        onClientConnected: {
            if(webSocket.status === WebSocket.Open){
                channel.connectTo(transport)
                webSocket.onTextMessageReceived.connect(transport.textMessageReceive)
                transport.onMessageChanged.connect(webSocket.sendTextMessage)
            }
        }
    }

    WebChannel {
        id: channel
        registeredObjects: [geographicData]
    }

    Component.onCompleted: {
        // Open the local storage database
        var db = LocalStorage.openDatabaseSync("Code_TYMPAN", "1.0", "Local Storage", 1000000);

        // Clear the contents of local storage
        db.transaction(function(tx) {
            tx.executeSql('DROP TABLE IF EXISTS ItemTable');
            tx.executeSql('CREATE TABLE IF NOT EXISTS ItemTable (key TEXT UNIQUE, value TEXT)');
        });

        // Load the contents of customConfig.json from qrc and store it in local storage
        var fileContent = Qt.resolvedUrl("qrc:/customConfig.json").toString();
        if (fileContent !== "") {
            db.transaction(function(tx) {
                tx.executeSql('INSERT INTO ItemTable VALUES (?, ?)', ["customConfig", fileContent]);
            });
            console.log("customConfig.json data stored in local storage.");
        } else {
            console.error("Failed to read customConfig.json.");
        }
    }
}
