#include <QApplication>
#include <QtWebView>
#include "TYMainWindow.h"

int main(int argc, char *argv[])
{
    QtWebView::initialize();
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QApplication app(argc, argv);

    // Create and show the main QML window
    TYMainWindow* _pMainWnd = new TYMainWindow();
    _pMainWnd->show();

    return app.exec();
}

#include "main.moc"
