var createMap = function () {
    // Création de la map
    var layerWMTS = L.geoportalLayer.WMTS({
                                              layer : "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2",
                                              apiKey : "cartes"
                                          });

    var layerWMS = L.geoportalLayer.WMS({
                                            layer : "SCAN1000_PYR-JPEG_WLD_WM",
                                            apiKey : "ign_scan_ws"
                                        });

    map  = L.map('map', {
                     zoom : 10,
                     center : L.latLng(45.2, 5.7)
                 });

    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    var drawControl = new L.Control.Draw({
                                             edit: false,
                                             draw : {
                                                 polygon: false,
                                                 marker: false,
                                                 polyline: false,
                                                 circle: false,
                                                 circlemarker: false
                                             }
                                         });
    map.addControl(drawControl);

    map.on(L.Draw.Event.CREATED, function (event) {
        var layer = event.layer;
        var latLngs = layer.getLatLngs()[0];

        // Convert each coordinate to EPSG:3857
        var transformed_coordinates = latLngs.map(function(coord) {
            var latlng = L.latLng(coord.lat, coord.lng);
            var projected = L.CRS.EPSG3857.project(latlng);
            return [projected.x, projected.y];
        });

        // Create the bounding box in EPSG:3857 format
        bbox.epsg_3857 = transformed_coordinates[0]+','+transformed_coordinates[2];
        console.log('bbox EPSG:3857 : ', bbox.epsg_3857);

        document.getElementById("coordinates").value = JSON.stringify(bbox.epsg_3857);

        // Convert each coordinate to EPSG:4326
        transformed_coordinates = latLngs.map(function(coord) {
            var latlng = L.latLng(coord.lat, coord.lng);
            var projected = L.CRS.EPSG4326.project(latlng);
            return [projected.y, projected.x];
        });

        // Create the bounding box in EPSG:4326 format
        bbox.epsg_4326 = transformed_coordinates[0]+','+transformed_coordinates[2];
        console.log('bbox EPSG:4326 : ', bbox.epsg_4326);

        toggleOkButton();
    });

    layerWMTS.addTo(map);
    layerWMS.addTo(map);

    var crs_value = map.options.crs.code;
    console.log("CRS:", crs_value);
    document.getElementById("crs").value = crs_value;
};

// Retrieve the custom config JSON from local storage
var customConfigJson = localStorage.getItem("customConfig");

Gp.Services.getConfig({
                          customConfigFile: customConfigJson,
                          timeOut: 20000,
                          onSuccess: createMap,
                          onFailure: function (e) {
                              console.error(e);
                          }
                      });
