//BEGIN SETUP
function output(message) {
    var output = document.getElementById("output");
    output.innerHTML = output.innerHTML + message + "\n";
}
// define image resolution
function computeHeight(x1, x2, y1, y2, IMGWidth) {
    var width = Math.abs(x2 - x1);
    var height = Math.abs(y2 - y1);
    var IMGHeight = IMGWidth * height / width;
    return IMGHeight;
}

// Function to convert image URL to base64
function imageURLToBase64(imageUrl) {
    return new Promise((resolve, reject) => {
                           fetch(imageUrl)
                           .then(response => {
                                     if (!response.ok) {
                                         throw new Error('Network response was not ok');
                                     }
                                     return response.blob();
                                 })
                           .then(blob => {
                                     const reader = new FileReader();
                                     reader.onloadend = () => {
                                         if (reader.result) {
                                             // Remove data URL prefix
                                             const base64Image = reader.result.replace(/^data:image\/(png|jpg|jpeg);base64,/, '');
                                             resolve(base64Image);
                                         } else {
                                             reject(new Error('Failed to read image data'));
                                         }
                                     };
                                     reader.readAsDataURL(blob);
                                 })
                           .catch(error => {
                                      console.error('Error converting image URL to base64:', error);
                                      reject(error);
                                  });
                       });
}
window.onload = function() {
    if (location.search != "")
        var baseUrl = (/[?&]webChannelBaseUrl=([A-Za-z0-9\-:/\.]+)/.exec(location.search)[1]);
    else
        var baseUrl = "ws://localhost:49327";

    output("Connecting to WebSocket server at " + baseUrl + ".");
    var socket = new WebSocket(baseUrl);

    socket.onclose = function() {
        console.error("web channel closed");
    };
    socket.onerror = function(error) {
        console.error("web channel error: " + error);
    };
    socket.onopen = function() {
        output("WebSocket connected, setting up QWebChannel.");
        new QWebChannel(socket, function(channel) {
            // make core object accessible globally
            window.core = channel.objects.core;
            coordinates.innerHTML = core.coordinates;
            document.getElementById("okButton").onclick = handleOkButtonClick;
            document.getElementById("cancelButton").onclick = handleCancelButtonClick;
            core.sendCoordinates.connect(function(message) {
                output("Received coordinates-" + core.coordinates + " : " + message);
            });
            output("Connected to WebChannel, ready to send/receive messages!");
        });
    }
}

async function handleOkButtonClick() {
    try {
        // Send coordinates to backend
        var input = document.getElementById("coordinates");
        var text = input.value;
        if (!text) {
            return;
        }
        output("Sent coordinates: " + text );
        core.receiveCoordinates(text);

        // Send crs to backend
        input = document.getElementById("crs");
        text = input.value;
        if (!text) {
            return;
        }
        output("Sent crs: " + text );
        core.receiveCrs(text);

        // Send background image to backend
        var values = bbox.epsg_3857.split(',');
        var x1 = parseFloat(values[0]);
        var y1 = parseFloat(values[1]);
        var x2 = parseFloat(values[2]);
        var y2 = parseFloat(values[3]);
        var IMGWidth = 1000;
        var IMGHeight = computeHeight(x1, x2, y1, y2, IMGWidth);
        const imageUrl = 'https://data.geopf.fr/wms-r?LAYERS=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2&FORMAT=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=&CRS=EPSG:3857&BBOX='+bbox.epsg_3857+'&WIDTH='+IMGWidth+'&HEIGHT='+IMGHeight+'';
        const base64Image = await imageURLToBase64(imageUrl);
        output("Sent background image");
        core.receiveImg(base64Image)

        // Send level curves to backend
        // The following request returns too many level curves. BBOX filter problem on WFS request submited to IGN support the 2024.02.21
        // const levelCurvesUrl = 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ELEVATION.CONTOUR.LINE:courbe&BBOX='+bbox.epsg_4326+'&crs=EPSG:4326';
        // Build polygon in crs EPSG:4326
        values = bbox.epsg_4326.split(',')
        x1 = values[0];
        y1 = values[1];
        x2 = values[2];
        y2 = values[3];
        var polygon_4326 = '(' + x1 + ' ' + y1 + ', ' + x1 + ' ' + y2 + ', ' + x2 + ' ' + y2 + ', ' + x2 + ' ' + y1 + ', ' + x1 + ' ' + y1 + ')';
        const levelCurvesUrl = 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ELEVATION.CONTOUR.LINE:courbe&cql_filter=INTERSECTS(geom, POLYGON('+polygon_4326+'))&crs=EPSG:4326';
        const response = await fetch(levelCurvesUrl);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const xmlString = await response.text();
        output("Sent level curves");
        core.receiveLevelCurves(xmlString)

        // Emit signal indicating that all resources have been loaded successfully
        core.view_send_ok();
    } catch (error) {
        console.error('Error:', error);
    }

}

function handleCancelButtonClick() {
    core.cancel();
}

//END SETUP
