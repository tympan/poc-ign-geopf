// Function to enable/disable the OK button based on input value
function toggleOkButton() {
    // Get the input element
    var coordinatesInput = document.getElementById('coordinates');
    // Get the OK button
    var okButton = document.getElementById('okButton');

    if (coordinatesInput.value.trim() !== '') {
        okButton.disabled = false;
    } else {
        okButton.disabled = true;
    }
}
