#ifndef TYWEBSOCKETTRANSPORT_H
#define TYWEBSOCKETTRANSPORT_H

#include <QJsonDocument>
#include <QWebChannelAbstractTransport>

class TYWebSocketTransport : public QWebChannelAbstractTransport {
    Q_OBJECT
public:
    using QWebChannelAbstractTransport::QWebChannelAbstractTransport;
    Q_INVOKABLE void sendMessage(const QJsonObject &message) override;
    Q_INVOKABLE void textMessageReceive(const QString &messageData);

signals:
    void messageChanged(const QString &message);
};

#endif // TYWEBSOCKETTRANSPORT_H
